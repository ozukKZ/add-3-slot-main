spawn(function()
--all rights reversed to glacier:tm:

--utils
local slot_ptrs = setmetatable({}, { __mode = 'kv' });
local hotkeys = { [0]='Enum.KeyCode.Zero', 'Enum.KeyCode.One', 'Enum.KeyCode.Two', 'Enum.KeyCode.Three', 'Enum.KeyCode.Four', 'Enum.KeyCode.Five', 'Enum.KeyCode.Six', 'Enum.KeyCode.Seven', 'Enum.KeyCode.Eight', 'Enum.KeyCode.Nine' };
local n_hotkeys = 10;
local rhotkeys_map = getsenv(game.Players.LocalPlayer.PlayerGui.LocalScript).hotkeys;

--user settings
local slotCount = 3; --math.max(slotCount, n_hotkeys - maxNormalSlots);

--hooks&main
do
    local getnamecallmethod = getnamecallmethod;
    local mt = getrawmetatable(game);
    local oldnc = mt.__namecall;
    setreadonly(mt, false);
    mt.__namecall = newcclosure(function(self, og, re, ...)
        if (self.Name == 'GUI' and getnamecallmethod() == 'FireServer') then
            local sl = slot_ptrs[og];
            if (sl) then
                og.Image = re.Image;
                og.Namer.Text = re.Namer.Text;
                return;
            end;
        end;
        return oldnc(self, og, re, ...);
    end)    
    setreadonly(mt, true);
end;

local buildMoveSlots;
do
    local httpService = game:GetService('HttpService');
    local hotbar = game.Players.LocalPlayer.PlayerGui.HUD.Bottom.HotBar;
    local maxNormalSlots = '7';
    local tonumber = tonumber;
    local ipairs = ipairs;
    local bigSlot = hotbar[maxNormalSlots]; --x scalar must be the biggest and not autofire
    local button_repos_constant = UDim2.fromScale(0.1, 0);
    local hotbar_directions = {
        backward = UDim2.fromScale(-0.05, 0);
        forward = UDim2.fromScale(0.05, 0);
    };
    
    function buildMoveSlots(lim, i)
        i = (i and (i + 1) or 1);
        if (i <= lim) then
            local newSlot = bigSlot:Clone();
            local nI = ((maxNormalSlots + i) % n_hotkeys);
            newSlot.Name = httpService:GenerateGUID();
            newSlot.Order.Text = nI;
            newSlot.Position = newSlot.Position + button_repos_constant;
            newSlot.Parent = hotbar;
            for _, v in ipairs(hotbar:GetChildren()) do
                local orderLabel = v:FindFirstChild('Order');
                if orderLabel then
                    v.Position = v.Position + (tonumber(orderLabel.Text) and hotbar_directions.backward or hotbar_directions.forward);
                end;
            end;
            bigSlot = newSlot;
            return i, nI, newSlot; -- returned the real i because there was a special case
        end;
        bigSlot = hotbar[maxNormalSlots]; --revert
    end;
end;

for i, nI, currSlot in buildMoveSlots, slotCount do
    do
        local hotkeyName = hotkeys[nI];
        if hotkeyName then
            rhotkeys_map[hotkeyName] = currSlot.Name;
        end;
    end;
    slot_ptrs[currSlot] = true;
end;
end)
